##Wells Fargo HeQ Cobrowse Demo

###QuickStart

Simple app to separate voice & video from live Assist whilst still implementing co-browse.

1. A CafeX FAS & FCSDK server
2. To edit these files to point to the IP / FQDN of your CafeX server.
3. To deploy a webserver (e.g. apache) to host the files and run the php script.

You can read a more detailed article on a standard implementation [here](https://support.cafex.com/hc/en-us/articles/201777841-What-is-the--code-I-need-to-make-a-video-call-from-a-browser-)



